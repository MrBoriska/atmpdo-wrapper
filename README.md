## AtmPdo Wrapper

Удобная оболочка над PDO, поддерживающая типизованные плейсхолдеры.
О том, что это за зверь такой(типизованные плейсхолдеры). Читайте в [презентации Романа Шевченко на DevConf 2013](http://2013.devconf.ru/data/2013/presentation/24.pdf). 
По сути, данная оболочка есть реализация предложенной Романом функциональности на базе PDO.

### Использование:

На данный момент поддерживаются:

- ?s ("string") - строки (а также DATE, FLOAT и DECIMAL). 
- ?i ("integer") - целые числа. 
- ?b ("boolean") - булев тип данных. 
- ?n ("name") - имена полей и таблиц 
- ?p ("parsed") - для вставки уже обработанных частей запроса
- ?a ("array") - набор значений для IN (строка вида 'a','b','c')
- ?u ("update") - набор значений для SET (строка вида `field`='value',`field`='value')


Инициализация класса полностью совпадает с инициализацией класса PDO:

```
#!php
<?php
$db = new AtmPdo("mysql:dbname=$dbname;host=$dblocation;charset=$charset", $dbuser, $dbpasswd);
```



Класс добавляет новый метод prepare_improved(), который поддерживает типизованные плейсхолдеры. Пример использования:

```
#!php
<?php
$sth = $db->prepare_improved('SELECT * FROM users WHERE ?n = ?i', array("id", 2));
$sth->setFetchMode(PDO::FETCH_CLASS, 'UsersModule\ORM\UsersEntity');
$sth->execute();
$res = $sth->fetch(PDO::FETCH_CLASS);
```



Кроме того, класс содержит специальные методы, для выполнения запросов стандартного вида:

```
#!php
<?php
$res = $db->getRow('SELECT * FROM users WHERE ?n = ?i', array("id", 2));
$res = $db->getAll('SELECT * FROM users WHERE ?n = ?i', array("id", 2));
```


Пример использования типизованных именованных плейсхолдеров:
```
#!php
<?php
$res = $db->getRow('SELECT * FROM users WHERE ?n:name = ?i', array(':name'=>'id', 2));
// OR
$res = $db->getRow('SELECT * FROM users WHERE ?n:name = ?i', array(2, ':name'=>'id'));
// OR
$res = $db->getRow('SELECT * FROM users WHERE ?n:name = ?i', array(':name'=>'id',0 => 2));
```




### Лицензия:

Лицензия свободная, MIT
http://www.opensource.org/licenses/mit-license.php

Удачи в использовании) Приму пожелания и замечания в задачах к репозиторию.